package comp2931.cwk1;

import org.junit.Test;

import java.util.Calendar;

import org.junit.Before;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


/**
 * Unit tests for Data class
 */
public class DateTest
{
    private Calendar currentDate = Calendar.getInstance();
    private Date now;


    @Before
    public void setUp()
    {
        now = new Date(currentDate.get(Calendar.YEAR),
                currentDate.get(Calendar.MONTH),
                currentDate.get(Calendar.DAY_OF_MONTH));
    }


    @Test
    public void dateToString()
    {
        assertThat((new Date(1996, 1, 1)).toString(), is("1996-01-01"));
        assertThat((new Date(1996, 12, 16)).toString(), is("1996-12-16"));

    }


    @Test(expected = IllegalArgumentException.class)
    public void negativeYear()
    {
        new Date(-155, 2, 1);
    }


    @Test(expected = IllegalArgumentException.class)
    public void monthTooHigh()
    {
        new Date(1887, 20, 12);
    }


    @Test(expected = IllegalArgumentException.class)
    public void monthTooLow()
    {
        new Date(1887, 0, 10);
    }


    @Test(expected = IllegalArgumentException.class)
    public void dayTooLow()
    {
        new Date(1887, 10, 0);
    }


    @Test
    public void dayOutOfLeapFebRange()
    {
        new Date(2000, 2, 29);
    }


    @Test(expected = IllegalArgumentException.class)
    public void dayOutOfFebRange()
    {
        new Date(1887, 2, 29);
    }


    @Test(expected = IllegalArgumentException.class)
    public void dayOutOfJanRange()
    {
        new Date(1887, 1, 32);
    }


    @Test(expected = IllegalArgumentException.class)
    public void dayOutOfAprRange()
    {
        new Date(1887, 4, 31);
    }


    @Test
    public void equals()
    {
        Date firstJan = new Date(2007, 1, 1);

        assertTrue(firstJan.equals(firstJan));
        assertTrue(firstJan.equals(new Date(2007, 1, 1)));
        assertFalse(firstJan.equals(new Date(2001, 1, 1)));
        assertFalse(firstJan.equals(new Date(2007, 2, 1)));
        assertFalse(firstJan.equals(new Date(2007, 1, 7)));
        assertFalse(firstJan.equals("2007-01-01"));
    }


    @Test
    public void getDayOfYear()
    {
        assertThat((new Date(2001, 1, 1)).getDayOfYear(), is(1));
        assertThat((new Date(2001, 2, 1)).getDayOfYear(), is(31 + 1));
        assertThat((new Date(2001, 3, 12)).getDayOfYear(), is(31 + 28 + 12));
        assertThat((new Date(2001, 6, 17)).getDayOfYear(), is(168));
        assertThat((new Date(2001, 12, 9)).getDayOfYear(), is(365 - 22));
        assertThat((new Date(2001, 12, 31)).getDayOfYear(), is(365));
    }


    @Test
    public void leapYearHandling()
    {
        assertThat((new Date(400, 12, 31)).getDayOfYear(), is(366));
        assertThat((new Date(100, 12, 31)).getDayOfYear(), is(365));
        assertThat((new Date(4, 12, 31)).getDayOfYear(), is(366));
        assertThat((new Date(1, 12, 31)).getDayOfYear(), is(365));
    }


    @Test
    public void currentDateConstructor()
    {
        assertTrue((new Date()).equals(now));
    }
}
