// Class for COMP2931 Coursework 1

package comp2931.cwk1;

import java.util.Calendar;


/**
 * Simple representation of a date.
 *
 * <p>Date objects can be created to represent the current date or a date
 * specified by the user in years, months and days. Supports leap years</p>
 *
 * @author Valentina Tabunova, Nick Efford
 */
public class Date
{
    //Class constants
    private static final int NUM_OF_MONTHS = 12;
    private static final int NUM_OF_DAYS_JAN_TYPE = 31;
    private static final int NUM_OF_DAYS_APR_TYPE = 30;
    private static final int NUM_OF_DAYS_FEB_TYPE = 28;
    private static final int NUM_OF_DAYS_LEAP_FEB_TYPE = 29;

    private int year;
    private int month;
    private int day;



    /**
     * Creates a Date object representing the current date.
     */
    public Date()
    {
        Calendar now = Calendar.getInstance();
        year = now.get(Calendar.YEAR);
        month = now.get(Calendar.MONTH);
        day = now.get(Calendar.DAY_OF_MONTH);
    }


    /**
     * Creates a date using the given values for year, month and day.
     *
     * @param y Year
     * @param m Month
     * @param d Day
     * @throws IllegalArgumentException
     */
    public Date(int y, int m, int d) throws IllegalArgumentException
    {
        if (y < 0)
        {
            throw new IllegalArgumentException("years are out of range");
        }
        else if (m <= 0 || m > NUM_OF_MONTHS)
        {
            throw new IllegalArgumentException("months are out of range");
        }
        else if (d <= 0 || d > numberOfDaysInMonth(y,m))
        {
            throw new IllegalArgumentException("days are out of range");
        }
        else
        {
            year = y;
            month = m;
            day = d;
        }
    }


    /**
     * Returns the year component of this date.
     *
     * @return Year
     */
    public int getYear()
    {
        return year;
    }


    /**
     * Returns the month component of this date.
     *
     * @return Month
     */
    public int getMonth()
    {
        return month;
    }


    /**
     * Returns the day component of this date.
     *
     * @return Day
     */
    public int getDay()
    {
        return day;
    }


    /**
     * Provides a string representation of this date.
     * <p>
     * ISO 8601 format is used (YYYY-MM-DD).
     *
     * @return Date as a string
     */
    @Override
    public String toString()
    {
        return String.format("%04d-%02d-%02d", year, month, day);
    }


    /**
     * Indicates whether this date is equal to another object or not.
     * <p>
     * <p>Here equals means that another object is a Date
     * and has the same values as this date for all of the fields.</p>
     *
     * @param other Object being compared with this record
     * @return true if this object is equal to the other, false otherwise
     */
    @Override
    public boolean equals(Object other)
    {
        if (this == other) return true;
        if (!(other instanceof Date)) return false;

        Date that = (Date) other;

        if (year != that.year) return false;
        if (month != that.month) return false;
        if (day != that.day) return false;

        return true;
    }


    /**
     * Provides a number of the day in the year.
     * For example, 12th of January will return 12.
     *
     * @return the day of the year as an integer
     */
    public int getDayOfYear()
    {
        int result = 0;
        for (int i = 1; i < month; i++)
            result += numberOfDaysInMonth(year, i);

        return result + day;
    }


    /**
     * Returns number of days is a given month
     *
     * @param y The year
     * @param m The number of the month for which the number of days is returned
     * @return the amount of days in a particular month
     */
    private int numberOfDaysInMonth(int y, int m)
    {
        if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12)
            return NUM_OF_DAYS_JAN_TYPE;
        if (m == 2 && isLeapYear(y))
            return NUM_OF_DAYS_LEAP_FEB_TYPE;
        if (m == 2 && !isLeapYear(y))
            return NUM_OF_DAYS_FEB_TYPE;
        if (m == 4 || m == 6 || m == 9 || m == 11)
            return NUM_OF_DAYS_APR_TYPE;
        return 0;
    }

    /**
     * Finds out if the year is a leap year.
     *
     * A leap year is a year which can b evenly divided by 4.
     * If the year can be evenly divided by 100, it is NOT a leap year,
     * unless the year is also evenly divisible by 400.
     *
     * @param y The number of the year
     * @return true if it is a leap year, false otherwise
     */
    private boolean isLeapYear(int y)
    {
        if (y % 400 == 0) return true;
        else if(y % 100 == 0) return false;
        else if(y % 4 == 0) return true;
        else return false;
    }
}
